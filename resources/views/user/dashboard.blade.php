    @extends('layouts.app')


    @section('content')

    <div class="container">
    <div class="row">
    <div class="col-md-4 offset-md-4">
    <div class="card">
    <div class="card-body">
    <form action="{{route('post_user')}}" method="post" enctype="multipart/form-data">
    <div class="form-group">
    {{csrf_field()}}
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" placeholder="Enter Name">
    </div>
    <div class="form-group">
        <label for="Image">Image</label>
        <input type="file" name="image" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
    </div>
    </div>
    </div>


    <div class="">
    <ul>
    @foreach($alldata as $data)
    <li><img src="{{URL::to('images/',$data->user_image)}}" class="img-fluid" style="max-height:200px;"/></li>
    @endforeach
    </ul>
    </div>

    </div>

    @endsection