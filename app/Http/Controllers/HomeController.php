<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\userData;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function dashboard(){
        $alldata = userData::all();
        return view('user.dashboard',compact('alldata'));
    }
    public function postData(Request $request){
        
        $imageName = time() . '_' . $request['image']->getClientOriginalName();
        $request['image']->move(public_path('images'),$imageName);
        $data = new userData();
        $data->name = $request['name'];
        $data->user_image = $imageName;
        $data->save();
        return redirect()->back();
    }
}
